terraform {
  backend "s3" {
    region               = "eu-central-1"
    encrypt              = true
    bucket               = "s3-cloudfront-infra-state-prod"
    key                  = "tfstate"
    workspace_key_prefix = "prod"
  }
}
provider "aws" {
  region = "eu-central-1"
}


resource "aws_s3_bucket" "s3_bucket_front" {
  bucket = var.s3_bucket_name
  acl    = "public-read"
  policy = file("policy.json")
  website {
    index_document = "index.html"
    error_document = "error.html"
  }
}

resource "aws_cloudfront_distribution" "s3_distribution" {
  origin {
    domain_name = aws_s3_bucket.s3_bucket_front.bucket_regional_domain_name
    origin_id   = var.s3_bucket_name
  }

  enabled             = true
  is_ipv6_enabled     = true
  default_root_object = "index.html"

  default_cache_behavior {
    allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = var.s3_bucket_name

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "allow-all"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }
  restrictions {
    geo_restriction {
      restriction_type = "whitelist"
      locations        = ["DE", "PL"]
    }
  }
  tags = {
    Environment = "production"
  }

  viewer_certificate {
    cloudfront_default_certificate = true
  }
}



